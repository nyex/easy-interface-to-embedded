/**
 * @file eie_param.h
 * @author Jose C. Pereira (jpereira1330@gmail.com)
 * @date 17-07-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef INCLUDE_EIE_PARAM_H_
#define INCLUDE_EIE_PARAM_H_

/// ===========================================================
///     INCLUDES
/// ===========================================================

#include <stdbool.h>

/// ===========================================================
///     TYPES / DEFINES
/// ===========================================================

typedef struct stc_eie_param *P_EIE_PARAM, EIE_PARAM;

/// ===========================================================
///     FUNCTIONS <= MEMORY MANAGER
/// ===========================================================

/**
 * @brief   Aloca na memoria os parametros do sistema
 * @return  @c P_EIE_PARAM Retorna ponteiro da alocacao
 */
P_EIE_PARAM eie_param_new();

/**
 * @brief   Desaloca da memoria os parametros do sistema
 * @param   @c pParam[in]   Ponteiro contendo parametros do sistema
 */
bool eie_param_free(P_EIE_PARAM pParam);


#endif  // INCLUDE_EIE_PARAM_H_
