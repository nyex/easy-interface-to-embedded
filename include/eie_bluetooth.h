/**
 * @file eie_bluetooth.h
 * @author Jose C. Pereira (jpereira1330@gmail.com)
 * @date 17-07-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef INCLUDE_EIE_BLUETOOTH_H_
#define INCLUDE_EIE_BLUETOOTH_H_

#include "eie_param.h"

/**
 * @brief   
 * @param pParam 
 * @return true 
 * @return false 
 */
bool eie_bluetooth_init(P_EIE_PARAM pParam);

#endif  // INCLUDE_EIE_BLUETOOTH_H_
