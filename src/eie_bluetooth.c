/**
 * @file eie_bluetooth.c
 * @author Jose C. Pereira (jpereira1330@gmail.com)
 * @date 17-07-2022
 *
 * @copyright Copyright (c) 2022
 *
 * @ref https://github.com/espressif/esp-idf/blob/9b8c558e63d95b491355a22e69f247834e899b47/examples/bluetooth/bluedroid/ble/ble_spp_client/main/spp_client_demo.c
 * 
 */

/// ===========================================================
///     INCLUDES
/// ===========================================================

#include "eie_bluetooth.h"

#include <esp_bt.h>
#include "esp_system.h"
#include <esp_bt_device.h>
#include <esp_bt_main.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// ===========================================================
///     PRIVATE FUNCTIONS
/// ===========================================================

esp_bt_controller_config_t _internal_set_default_cfg();

/// ===========================================================
///     PUBLIC FUNCTIONS
/// ===========================================================

bool eie_bluetooth_init(P_EIE_PARAM pParam) {
    esp_bt_controller_config_t btCfg;

    if (pParam == NULL) {
        return false;
    }

    esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT);
    btCfg = _internal_set_default_cfg();
    esp_bt_controller_init(&btCfg);
    esp_bt_controller_enable(ESP_BT_MODE_BLE);
    esp_bluedroid_init();
    esp_bluedroid_enable();
 
    /// ble_client_appRegister
    /// spp_uart_init();

    return true;
}

/// ===========================================================
///     PRIVATE FUNCTIONS
/// ===========================================================

esp_bt_controller_config_t _internal_set_default_cfg() {
    esp_bt_controller_config_t bt_cfg;

    /// Configuracoes padrao
    bt_cfg.controller_task_stack_size = ESP_TASK_BT_CONTROLLER_STACK;
    bt_cfg.controller_task_prio = ESP_TASK_BT_CONTROLLER_PRIO;
    bt_cfg.hci_uart_no = BT_HCI_UART_NO_DEFAULT;
    bt_cfg.hci_uart_baudrate = BT_HCI_UART_BAUDRATE_DEFAULT;
    bt_cfg.scan_duplicate_mode = SCAN_DUPLICATE_MODE;
    bt_cfg.scan_duplicate_type = SCAN_DUPLICATE_TYPE_VALUE;
    bt_cfg.normal_adv_size = NORMAL_SCAN_DUPLICATE_CACHE_SIZE;
    bt_cfg.mesh_adv_size = MESH_DUPLICATE_SCAN_CACHE_SIZE;
    bt_cfg.send_adv_reserved_size = SCAN_SEND_ADV_RESERVED_SIZE;
    bt_cfg.controller_debug_flag = CONTROLLER_ADV_LOST_DEBUG_BIT;
    bt_cfg.mode = BTDM_CONTROLLER_MODE_EFF;
    bt_cfg.ble_max_conn = CONFIG_BTDM_CTRL_BLE_MAX_CONN_EFF;
    bt_cfg.bt_max_acl_conn = CONFIG_BTDM_CTRL_BR_EDR_MAX_ACL_CONN_EFF;
    bt_cfg.bt_sco_datapath = CONFIG_BTDM_CTRL_BR_EDR_SCO_DATA_PATH_EFF;
    bt_cfg.auto_latency = BTDM_CTRL_AUTO_LATENCY_EFF;
    bt_cfg.bt_legacy_auth_vs_evt = BTDM_CTRL_LEGACY_AUTH_VENDOR_EVT_EFF;
    bt_cfg.bt_max_sync_conn = CONFIG_BTDM_CTRL_BR_EDR_MAX_SYNC_CONN_EFF;
    bt_cfg.ble_sca = CONFIG_BTDM_BLE_SLEEP_CLOCK_ACCURACY_INDEX_EFF;
    bt_cfg.pcm_role = CONFIG_BTDM_CTRL_PCM_ROLE_EFF;
    bt_cfg.pcm_polar = CONFIG_BTDM_CTRL_PCM_POLAR_EFF;
    bt_cfg.hli = BTDM_CTRL_HLI;
    bt_cfg.magic = ESP_BT_CONTROLLER_CONFIG_MAGIC_VAL;

    return bt_cfg;
}
