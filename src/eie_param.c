/**
 * @file eie_param.c
 * @author Jose C. Pereira (jpereira1330@gmail.com)
 * @date 17-07-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/// ===========================================================
///     INCLUDES
/// ===========================================================

#include "eie_param.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <nvs_flash.h>          /// Non-Volatile Stoage

/// ===========================================================
///     TYPES / DEFINES
/// ===========================================================

struct stc_eie_param{
    bool started;
};

/// ===========================================================
///     FUNCTIONS <= PRIVATE
/// ===========================================================

void _initialize_default_config() {
    nvs_flash_init();
}

/// ===========================================================
///     FUNCTIONS <= MEMORY MANAGER
/// ===========================================================

P_EIE_PARAM eie_param_new() {
    P_EIE_PARAM param;

    /// Alocando espaco na memoria
    param = (P_EIE_PARAM) malloc(sizeof(EIE_PARAM));
    if (param == NULL) {
        return NULL;
    }

    /// Limpando estrutura
    memset(param, 0, sizeof(EIE_PARAM));

    /// Inicializando configuracoes padrao
    _initialize_default_config();

    return param;
}

bool eie_param_free(P_EIE_PARAM pParam) {

    if (pParam == NULL)
        return true;
    
    /// Limpando a memoria e desalocando
    memset(pParam, 0, sizeof(EIE_PARAM));
    free(pParam);

    return true;
}