/**
 * @file main.c
 * @author Jose C. Pereira (jpereira1330@gmail.com)
 * @date 17-07-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "eie_param.h"
#include "eie_bluetooth.h"

void app_main() {

    P_EIE_PARAM param;
    
    param = eie_param_new();
    if (param == NULL) {
        return;
    }

    eie_bluetooth_init(param);

    eie_param_free(param);

}
